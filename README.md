# Random-Background-Colors
A random background colors with JavaScript !

This is a little web page which displays random background colors with linear-gradient in CSS.

Here is an example : 

![](img/backgoundGreen.PNG )

And another one : 

![image](img/backgroundPurple.PNG)


I did it for fun !
